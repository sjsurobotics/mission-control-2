
# Dependencies
https://nodejs.org/en/

# Getting Started
Initial setup will take a while, but development should be quick! (Henry is bae)
  
1. Run `npm install -g grunt-cli`
2. Run `npm install -g bower`
3. CD into the project directory.
4. Run `npm install .`
5. Run `bower install`
6. Run `grunt`
  
Open up http://localhost:8080/ or http://localhost:1337/ depending on which branch you are on.
  
# Rover Simulation
Open up http://localhost:8080/simulator.html or http://localhost:1337/simulator.html
  
# Testing
Manual tests are written here:  
[Google Spreadsheet](https://docs.google.com/spreadsheets/d/1pZUGWvi2Z5PbpGa9JUpuhGOmqEp02FHSKgU2dTz8Sok/edit?usp=drive_web)
