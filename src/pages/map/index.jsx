
import NetHandler from "../../network/NetHandler.jsx";
var TestPage = React.createClass({

	/* Create your Model */
	getInitialState: function() {
		return {count:0, x:0, y:0, markerToggle: false};
	},

	/* Render our View */
	render: function() {
		return (
			<div>	
			
				<div className="row">
					<div className="col-xs-9">
						<div ref="map" onClick = {this.addMarker} style={{height:"500px", width:"100%"}}></div>

						<a className="btn btn-info" role= "button" onClick = {this.goToLocation}>San Jose</a>
						<a className="btn btn-info" role= "button" onClick = {this.goToCowdung}>Cow Dung</a>
						<a className="btn btn-primary" onClick={this.goToMarker}>Jump To Rover</a>
						<a className="btn btn-primary" onClick={this.addMarkerToggle}>{this.state.markerToggle ? "Click On Map to Add Marker" : "Add Marker"}</a>
						<input style={{display: 'none', width: '100px'}} type="text" ref="markerText" className="form-control"/>
					</div>
					<div className="col-xs-3">				
						<strong>Longitude:</strong> {this.safeMapData().long} <br />
						<strong>Latitude:</strong> {this.safeMapData().lat} <br />
						<strong>Altitude:</strong> {this.safeMapData().alt} {this.safeMapData().alt_unit} <br />
						<strong>Satellites:</strong> {parseFloat(this.safeMapData().number_of_satellites)} <br />
						<strong>Horiz Confidence:</strong> {parseFloat(this.safeMapData().horiz_confidence)} {this.confidenceRating()} <br />
					</div>
				</div>

			</div>
		)
	},

	confidenceRating: function() {
		var conf = parseFloat(this.safeMapData().horiz_confidence);
		if (conf < 1) {
			return "Ideal";
		} else if (conf < 2) {
			return "Excellent";
		} else if (conf < 5) {
			return "Good";
		} else if (conf < 10) {
			return "Moderate";
		} else if (conf < 20) {
			return "Fair";
		} else {
			return "Poor";
		}
	},

	safeMapData: function() {
		try {
			return this.state["GPS"].value;
		} catch(e) {
			return {
				lat: '37º20.22370',
				latDir: 'N',
				long: '121º52.89374',
				longDir: 'W',
				alt: '46.4',
				alt_unit: 'M',
				horiz_confidence: '1.12',
				number_of_satellites: '08'
			}
		}
	},

	addMarkerToggle: function() {
		this.setState({
			markerToggle: !this.state.markerToggle
		});
		if (!this.state.markerToggle) {
			this.refs.markerText.style.display = 'inline-block';
		} else {
			this.refs.markerText.style.display = 'none';
		}
	},

	componentDidMount: function() {

		var roverIcon = L.icon({
			iconUrl: 'images/rover_marker.png',
			iconSize: [64, 64],
			iconAnchor: [32, 64],
			popupAnchor:  [-3, -76]
		});

		var pingIcon = L.icon({
			iconUrl: 'images/pin.png',
			iconSize: [32, 32],
			iconAnchor: [16, 32],
			popupAnchor:  [0, -18]
		});

		this.pingIcon = pingIcon;

	  this.map = L.map(this.refs.map).setView([0, 0], 13);
		this.marker = L.marker([0, 0], {icon: roverIcon}).addTo(this.map);

		L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    			attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
			}).addTo(this.map);

		// L.tileLayer('http://khm1.googleapis.com/kh?v=203&hl=en-US&&x={x}&y={y}&z={z}', {
  //   			attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
		// 	}).addTo(this.map);

		L.tileLayer('/static_map/{z}/{x}/{y}.png', {
    			attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
			}).addTo(this.map);

		
    	
		this.markers = {};

		this.netHandler = new NetHandler("tracker");
		this.netHandler.listen((changes) => {
		    
		    this.setState(changes);

		    try {

	    		var lat = changes["GPS"].value.lat.match(/([0-9]+)º([0-9.]+)/i);
	    		lat = parseFloat(lat[1]) + parseFloat(lat[2])/60;
		    	if (changes["GPS"].value.latDir == "S") {
		    		lat = -lat;
		    	}

		    	var long = changes["GPS"].value.long.match(/([0-9]+)º([0-9.]+)/i);
	    		long = parseFloat(long[1]) + parseFloat(long[2])/60;
		    	if (changes["GPS"].value.longDir == "W") {
		    		long = -long;
		    	}

		    	this.setState({
		    		calcLong: long,
		    		calcLat: lat
		    	})

			    this.marker.setLatLng([lat, long]);
		    } catch(e) { console.log(e) }

		    if (changes.executeCommand) {
		    	console.log(changes.executeCommand);
		    	if (changes.executeCommand.name == "addPing") {
		    		this.createMarker(changes.executeCommand.id, changes.executeCommand.lat, changes.executeCommand.long, changes.executeCommand.text);
		    	}

		    	if (changes.executeCommand.name == "removePing") {
		    		this.removeMarker(changes.executeCommand.id);
		    	}
		    }
		});



	},

	componentWillUnmount: function() {
		clearInterval(this.sendUpdatesInterval);
	},

	sendUpdates: function() {
		this.netHandler.execute("Tracker", {
			"mode": "moveAngle",
			"yaw": this.state.x,
			"pitch": this.state.y
		});
	},

	//these fucntions are for manual button control if the intrface does not work(Priank's work goes here) . 
	goToLocation: function(event){
		this.map.panTo([37.3382,-121.8863]);
	},

	goToCowdung: function(event){
		this.map.panTo([38.4064094,-110.7918953]);
	},

	addMarker: function(event){
		if (this.state.markerToggle) {
			this.addMarkerToggle();
		} else {
			return;
		}

		let id = (new Date()).getTime();

		var latLong = this.map.mouseEventToLatLng(event); 

		this.netHandler.execute("Pinger", {
			"command": "add",
			"id": id,
			"long": latLong.lng,
			"lat": latLong.lat,
			"text": this.refs.markerText.value
		});

		this.refs.markerText.value = "";
		// this.createMarker(id, latLong.lat, latLong.lng);
	},

	createMarker: function(id, lat, long, text) {

		console.log(id, lat, long)
		console.log(this.markers);
		if (this.markers[id]) { return; }
		console.log('hey');

		this.newMarker = new L.marker([lat, long], {icon: this.pingIcon}).addTo(this.map);

		var popUp = $("<div>lat: " + lat + "<br />long: " + long + "<br/>" + text + "<br /><button class='btn btn-primary'>Remove</button></div>");
		var removeButton = $("button", popUp);
		removeButton.click(() => {
			this.netHandler.execute("Pinger", {
				"command": "remove",
				"id": id
			});
			this.removeMarker(id);
		});

		this.newMarker.bindPopup(popUp[0]);
		this.markers[id] = this.newMarker;

	},

	removeMarker: function(id) {
		if (this.markers[id]) {
			this.map.removeLayer( this.markers[id] );
		}
	},
	
	goToMarker: function() {
		this.map.panTo([this.state.calcLat || 0,this.state.calcLong || 0]);
	},

	upClick: function(event) {

		this.setState({ 
			count:this.state.count +1
		});
	},

	downClick: function(event){ 
		this.setState({
			count:this.state.count -1
		});
	},

	left: function() {
	},

	right: function() {
	},

	centerCam: function() {
	},

	handleTest: function(event) {
		alert("HELLO");
	}, 	

});

export var route = {
	name: "Map",  
	link: "#/map",
};


/* Export our newly created page so that the world can see it! */
export default TestPage;

