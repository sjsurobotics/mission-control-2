import NetHandler from "../../network/NetHandler.jsx";

var KitchenSink = React.createClass({
  componentDidMount: function() {
    this.netHandler = new NetHandler('kitchenSink');

    this.netHandler.listen((changes) => {
      this.setState(changes);
    });
  },
  componentWillUnmount: function() {
      if (this.netHandler) {
          this.netHandler.close();
      }
  },
  getInitialState: function() {
    return {};

    return {
      'ArmPosition': {
        value: 'N/A'
      },

      'Temperature Readings': {
        value: {
          'Temperature1': 0,
          'Temperature2': 0,
          'Temperature3': 0,
          'Temperature4': 0,
          'Temperature5': 0
        }
      },

      'Temperature Limits': {
        value: {
          'Upper': 0,
          'Lower': 0
        }
      },

      'Fan Speed': {
        value: {
          'Fan1': 0,
          'Fan2': 0,
          'Fan3': 0,
          'Fan4': 0,
        }
      },

      'GPS': {
        value: {
          'lat': 0,
          'latDir': 0,
          'long': 0,
          'longDir': 0,
          'alt': 0,
          'alt_unit': 0,
          'horiz_confidence': 0,
          'number_of_satellites': 0
        }
      },

      'SENSORS': {
        value: {
          'moisture': 0
        }
      },

      'MPU': {
        value: {
          'roll': 0,
          'pitch': 0,
          'temperature': 0,
          'heading': 0
        }
      },

      'MPU2': {
        value: {
          'xAngle': 0,
          'yAngle': 0,
          'temperature': 0
        }
      },

      'LIDAR': {
        value: {
          'lidarMeasurement': 0
        }
      },

      'CAMERA GIMBAL': {
        value: {
          'yaw': 0,
          'pitch': 0
        }
      }
    }
  },
  render: function() {
    return (
    <div>
      {(() => {
        var arr = [];
        for (var key in this.state) {
          var model = this.state[key].value;
          arr.push(<div className="col-xs-4" key={key}>
            <h4>{key}</h4>
            {
              (() => {
                if (typeof model == "object") {
                  var arr2 = [];

                  for (var k2 in model) {
                    arr2.push(<div key={k2}>
                      <p><strong>{k2}</strong>: {model[k2]}</p>
                    </div>);
                  }

                  return arr2;
                } else {
                  return <p>{model}</p>;
                }
              })()
            }
          </div>);
        }
        return arr;
      })()}
    </div>
    );
  }
});

export var route = {
  name: "Kitchen Sink",
  link: "#/kitchensink"
};

/* Export our newly created page so that the world can see it! */
export default KitchenSink;