import KnobButton from '../../components/KnobButton.jsx';
import ProgressBar from '../../components/ProgressBar.jsx';
import Slider from '../../components/Slider.jsx';
import Toggle from '../../components/Toggle.jsx';
import NetHandler from "../../network/NetHandler.jsx";

/*keydown stuff from http://stackoverflow.com/questions/28102746/how-to-trigger-keypress-event-in-react-js*/

var HomePage = React.createClass({

  componentWillMount:function() {
    this.netHandler = new NetHandler("navi");
    this.netHandler.listen((changes) => {
      this.setState(changes);
      console.log(changes);
      // console.log(this.state);
    })
    // console.log(this.netHandler);
    document.addEventListener("keyup", this._handleKey, false);
    this.sendState = setInterval(() => {
      this.netHandler.execute("Arm", {
        "rotonda": 0,
        "base": 0,
        "elbow": 0,
        "pitch": this.state.handAngle,
        "method": this.state.method,
        "roll": this.state.rollAngle,
        "claw": this.state.clawForce,
        "laser": 1,
      });

      if (this.state.method == 0) {
        this.setState({
          pitchOrRoll: 'pitch'
        })
      } else if (this.state.method == 4) {
        this.setState({
          pitchOrRoll: 'roll'
        })
      } else if (this.state.method == -3) {
        this.setState({
          rollDisplay: '<<<'
        })
      } else if (this.state.method == -2) {
        this.setState({
          rollDisplay: '<<'
        })
      } else if (this.state.method == -1) {
        this.setState({
          rollDisplay: '<'
        })
      } else if (this.state.method == 3) {
        this.setState({
          rollDisplay: '>>>'
        })
      } else if (this.state.method == 2) {
        this.setState({
          rollDisplay: '>>'
        })
      } else if (this.state.method == 1) {
        this.setState({
          rollDisplay: '>'
        })
      }

    }, 100);
  },

  componentWillUnmount: function() {
      this.netHandler.close();
      clearInterval(this.sendState);
  },

  /* Create our Model */
  getInitialState: function() {
    return {
      progress: 90,
      inputMode: "joystick",
      carMode: "car",

      rollDisplay: "N/A",

      // arm
      method: 0,
      handAngle: 0,
      rollAngle: 180,
      clawForce: 999,
    };
    // return {progress: 90, direction: "Forward"};
  },

  /* Render our View */
  render: function() {
    // console.log(this.netHandler);
    // console.log(this);
    return (
    <div id="everything"> 
      <div className="row">
        <div className="col-xs-9">

          <div className="row">
            <div className="col-xs-4">
              <p> Wrist Pitch Angle {this.state.handAngle} </p>
              <Slider initialValue={this.state.handAngle} min="-90" max="90" hideTooltip="true" onChange={this.onHandAngleChange}/>
            </div>
            <div className="col-xs-4">
              <p> Wrist Roll Angle {this.state.rollAngle} </p>
              <Slider initialValue={this.state.rollAngle} min="0" max="360" hideTooltip="true" onChange={this.onRollAngleChange}/>
            </div>
            <div className="col-xs-4">
              <p> Wrist Claw Position {this.state.clawForce} </p>
              <Slider initialValue={this.state.clawForce} min="0" max="999" hideTooltip="true" onChange={this.onClawChange}/>
            </div>
          </div>
          <br />
          <div className="row">
            <div className="col-xs-2">
              <button onClick={this.methodEvent} method="-3" className="btn btn-info btn-block">Left Roll High Speed</button>
            </div>
            <div className="col-xs-2">
              <button onClick={this.methodEvent} method="-2" className="btn btn-info btn-block">Left Roll</button>
            </div>
            <div className="col-xs-2">
              <button onClick={this.methodEvent} method="-1" className="btn btn-info btn-block">Left Roll Slow</button>
            </div>
            <div className="col-xs-2">
              <button onClick={this.methodEvent} method="1" className="btn btn-default btn-block">Right Roll Slow</button>
            </div>
            <div className="col-xs-2">
              <button onClick={this.methodEvent} method="2" className="btn btn-default btn-block">Right Roll</button>
            </div>
            <div className="col-xs-2">
              <button onClick={this.methodEvent} method="3" className="btn btn-default btn-block">Right Roll High Speed</button>
            </div>
          </div>
          <br />
          <div className="row">
            <div className="col-xs-6">
              <button onClick={this.methodEvent} method="0" className={"btn btn-" + (this.state.pitchOrRoll == "pitch" ? "primary" : "default") + " btn-block"}>Pitch Only</button>
            </div>
            <div className="col-xs-6">
              <button onClick={this.methodEvent} method="4" className={"btn btn-" + (this.state.pitchOrRoll == "roll" ? "primary" : "default") + " btn-block"}>Roll Only</button>
            </div>
          </div>

          <br />

          <div className="row">
            <div className="col-xs-4">
              <button onClick={this.methodEvent} method="5" className="btn btn-default btn-block">Servos Off</button>
            </div>
            <div className="col-xs-4">
              <button onClick={this.methodEvent} method="6" className="btn btn-warning btn-block">50% Power</button>
            </div>
            <div className="col-xs-4">
              <button onClick={this.methodEvent} method="7" className="btn btn-success btn-block">100% Power</button>
            </div>
          </div>

          <div className="row">
            <div className="col-xs-6">
              {this.state.roverOnline ? "Connected to Rover" : "Not Connected!!!" }
              <br />
              Claw Force: {this.state.clawForce}
              <br />
              Method: {this.state.method}
              <br />
              {this.state.pitchOrRoll} only
              <h1>Roll: { this.state.rollDisplay }</h1>
            </div>
            <div className="col-xs-3">
              <img src={this.state.clawForce < 300 ? "/images/closed.jpg" : "/images/open.jpg"} />
            </div>
            <div className="col-xs-3">
              <br />
              <img height="100px" src={"/images/" + this.state.carMode + ".png"}/>
            </div>
          </div>

          <hr />


          <div>
            {(() => {
              var arr = [];
              for (var key in this.state) {
                var model = this.state[key].value;
                if (model) {
                  arr.push(<div className="col-xs-4" key={key}>
                    <h4>{key}</h4>
                    {
                      (() => {
                        if (typeof model == "object") {
                          var arr2 = [];

                          for (var k2 in model) {
                            arr2.push(<div key={k2}>
                              <p><strong>{k2}</strong>: {model[k2]}</p>
                            </div>);
                          }

                          return arr2;
                        } else {
                          return <p>{model}</p>;
                        }
                      })()
                    }
                  </div>);
                }
              }
              return arr;
            })()}
          </div>


        </div>
        <div className="col-xs-3">
          <center>
              <div className="btn-group">
                <button type="button" className={"btn " + this.inputModeClass("manual")} onClick={() => this.setInputMode("manual")}>Manual</button>
                <button type="button" className={"btn " + this.inputModeClass("joystick")} onClick={() => this.setInputMode("joystick")}>Joystick</button>
              </div><br/><br/>
              <div id="buttons">
                <JoystickControl inputMode={this.state.inputMode} carMode={this.state.carMode} netHandler={this.netHandler} setMode={this.setMode} setState={(state) => this.setState(state)}/>
                <WASD ref="wasd" netHandler={this.netHandler} carMode={this.state.carMode} setMode={this.setMode}/>
              </div>
            </center>
        </div>
      </div>
    </div>    
    )
  },

  inputModeClass: function(input) {
    return this.state.inputMode == input ? "btn-primary" : "btn-default";
  },

  setInputMode: function(input) {
    this.setState({
      "inputMode": input
    })
  },

  setMode: function(carMode) {
    this.setState({
      "carMode": carMode
    })
  },

  /* Controller Functions */

  onSliderChange: function(value) {
    this.setState({
      progress: value.newValue
    });
  },



  // Arm shit

  methodEvent: function(button) {
    this.setState({ 
      method: parseInt(button.target.attributes.method.nodeValue)
    });
  },

  _handleKey:function(event){
        //console.log(event);
        const UP = 40;
        const DOWN = 38;
        const RIGHT = 39;
        const LEFT = 37;
        const MOVE = 10;
        const CTRL = 17;
        const ALT = 18;
        switch(event.keyCode) {
          case UP:
            if(this.state.handAngle+MOVE >= 90) {
          this.setState({ handAngle: 90 });
            } else {
              this.setState({ handAngle: this.state.handAngle+MOVE});
            }
            console.log(this.state.handAngle);
            break;
          case DOWN:
            if(this.state.handAngle-MOVE <= -90) {
              this.setState({ handAngle: -90});
            } else {
              this.setState({ handAngle: this.state.handAngle-MOVE});
            }
            console.log(this.state.handAngle);
            break;
          case RIGHT:
            if(this.state.rollAngle+MOVE >= 360) {
              this.state.rollAngle = 360;
            } else {
              this.state.rollAngle += MOVE;
            }
            console.log(this.state.rollAngle);
            break;
          case LEFT:
            if(this.state.rollAngle-MOVE <= 0) {
              this.state.rollAngle = 0;
            } else {
              this.state.rollAngle -= MOVE;
            }
            console.log(this.state.rollAngle);
            break;
          case CTRL:
            // GRAB SOMETHING
            this.state.clawForce = 0;
            console.log(this.state.clawForce);
            break;
          case ALT:
            // GRAB SOMETHING
            this.state.clawForce = 999;
            console.log(this.state.clawForce);
            break;
          default:
            console.log("invalid keyinput ", event.keyCode);
            break;
        }
   },


  toggleManual: function(enabled) {
    if (enabled) {
      this.refs.manualControl.style.color = "inherit";
      this.refs.manualControl.style.opacity = 1;
      this.refs.manualControl.style.pointerEvents = "auto";
    } else {
      this.refs.manualControl.style.color = "#333";
      this.refs.manualControl.style.opacity = 0.5;
      this.refs.manualControl.style.pointerEvents = "none";
    }
  },

  onToggleManualControl: function(e) {
    this.toggleManual(e.newValue);
    this.setState({
      manualControl: e.newValue
    })
  },


  onHandAngleChange: function(value) {
    this.setState({
      handAngle: value.newValue
    })
  },
  onRollAngleChange: function(value) {
    console.log(value);
    this.setState({
      rollAngle: value.newValue
    })
  },
  onClawChange: function(value) {
    console.log(value);
    this.setState({
      clawForce: value.newValue
    })
  },

});

var WASD = React.createClass({
  /*<WASD netHandler={this.netHandler}/>*/
propTypes: {
},

componentDidMount: function() {
    $(document.body).on('keydown', this.handleKeyDown);
    $(document.body).on('keyup', this.handleKeyUp);
},

componentWillUnmount: function() {
    $(document.body).off('keydown', this.handleKeyDown);
    $(document.body).off('keyup', this.handleKeyDown);
},

getInitialState: function() {
    return{
    left: 0,
    right: 0,
    up: 0,
    down: 0,
      value: 'hi',
      rovermode: "car",
      speedGo: 25,
      speedStop: 0, 
      moving: 0
      //moving = positive if rover is in motion, stopped = 0, back = negative
    };
},

onChange: function(event) {
    this.setState({ value: event.target.value }, function() {});
},

handleKeyDown: function(e) {
  // console.log(this)
  // console.log(this.props)
    var ENTER = 13;
    var A = 65;
    var LEFT = 37;
    var W = 87;
    var UP = 38;
    var D = 68;
    var RIGHT = 39;
    var S = 83;
    var DOWN = 40;
    var J = 74;
    var K = 75;
    var L = 76;
    if( e.keyCode == ENTER) {
      this.setState({value: 'Enter key event triggered', left: 0, right: 0, up:0, down:0});
    } else if( e.keyCode == J) {
      this.setCarMode();
    } else if( e.keyCode == K) {
      this.setRotateMode();
    } else if( e.keyCode == L) {
      this.setCrabMode();
    } else if( e.keyCode == A || e.keyCode == LEFT) {
      this.setState({value: 'A/Left', left: this.state.left + 1})
      //rotate left
      this.props.netHandler.execute('driveSystem', {
        "name": "move",
        "mode": this.state.rovermode,
        "speed": this.state.speedGo,
        "angle": 180
      });
    } else if(e.keyCode == D || e.keyCode == RIGHT) {
      this.setState({value: 'D/Right', right: this.state.right + 1});
      //rotate right
      this.props.netHandler.execute('driveSystem', {
        "name": "move",
        "mode": this.state.rovermode,
        "speed": this.state.speedGo,
        "angle": 0
      });
    } else if(e.keyCode == W || e.keyCode == UP) {
      this.setState({value: 'W/Up', up: this.state.up + 1});
      //move forward
      // this.setState({moving: this.state.speedGo});
      this.props.netHandler.execute('driveSystem', {
        "name": "move",
        "mode": this.state.rovermode,
        "speed": this.state.speedGo,
        "angle": 90
      });
    } else if(e.keyCode == S || e.keyCode == DOWN) {
      this.setState({value: 'S/Down', down: this.state.down + 1});
      //move backward
      // this.setState({moving: -this.state.speedGo});
      this.props.netHandler.execute('driveSystem', {
        "name": "move",
        "mode": this.state.rovermode,
        "speed": (this.state.speedGo),
        "angle": 270
      });
    }

  },

  handleKeyUp: function(e) {
    var ENTER = 13;
    var A = 65;
    var LEFT = 37;
    var W = 87;
    var UP = 38;
    var D = 68;
    var RIGHT = 39;
    var S = 83;
    var DOWN = 40;
    if( e.keyCode == ENTER) {
      this.setState({value: 'Enter key event triggered', left: 0, right: 0, up:0, down:0});
    } else if( e.keyCode == A || e.keyCode == LEFT) {
      this.setState({value: 'A/Left', left: this.state.left + 1})
      //stop rotate left
      this.props.netHandler.execute('driveSystem', {
        "name": "move",
        "mode": this.props.carMode,
        "speed": this.state.moving,
        "angle": 0
      });
    } else if(e.keyCode == D || e.keyCode == RIGHT) {
      this.setState({value: 'D/Right', right: this.state.right + 1});
      //stop rotate right
      this.props.netHandler.execute('driveSystem', {
        "name": "move",
        "mode": this.props.carMode,
        "speed": this.state.moving,
        "angle": 0
      });
    } else if(e.keyCode == W || e.keyCode == UP) {
      this.setState({value: 'W/Up', up: this.state.up + 1});
      //stop move forward
      this.setState({moving: 0});
      this.props.netHandler.execute('driveSystem', {
        "name": "move",
        "mode": this.props.carMode,
        "speed": 0,
        "angle": 0
      });
    } else if(e.keyCode == S || e.keyCode == DOWN) {
      this.setState({value: 'S/Down', down: this.state.down + 1});
      //stop move backward
      this.setState({moving: 0});
      this.props.netHandler.execute('driveSystem', {
        "name": "move",
        "mode": this.props.carMode,
        "speed": 0,
        "angle": 0
      });
    }
  },

  setCarMode: function(event) {
    this.setState({
      rovermode : "car"
    });
    this.props.setMode("car");
    console.log("Car Mode Activated!");
  },
  setCrabMode: function(event) {
    this.setState({
      rovermode : "crabwalk"
    });
    this.props.setMode("crabwalk");
    console.log("Crabwalk Mode Activated!");
  },
  setRotateMode: function(event) {
    this.setState({
      rovermode: "rotate"
    });
    this.props.setMode("rotate");
    console.log("Rotate Mode Activated!");
  },
  setTankMode: function(event) {
    this.setState({
      rovermode: "tank"
    });
    this.props.setMode("tank");
    console.log("Tank Mode Activated!");
  },

  render: function() {
    return(
      <div className="WASD box">
        <div className="btn-group-vertical btn-group-md">
          <button className="btn btn-primary" onMouseDown={this.setCarMode} style={{width: '126px'}}>Car Mode</button>
          <button type="button" className="btn btn-primary" onMouseDown={this.setCrabMode} style={{width: '126px'}}>Crab Mode</button>
          <button type="button" className="btn btn-primary" onMouseDown={this.setRotateMode} style={{width: '126px'}}>Rotate Mode</button>
          <button type="button" className="btn btn-danger" style={{width: '126px'}}> Mode: {this.props.carMode}</button>
        </div>
      </div>
    );
  }
});

var Legend = React.createClass({
  render: function() {
    return(
      <div style={{height: "50%"}}>
        <h3><center>Legend</center></h3>
          <ul>
            <li>Car Mode    - J</li>
            <li>Rotate Mode   - K</li>
            <li>Crab Mode   - L</li>
            <li>Pirate Mode - P</li>
          </ul>
      </div>                
    );
  }
});

var JoystickControl = React.createClass({
  getDefaultProps: function() {
    return {
      carMode: "car"
    };
  },

  getInitialState: function() {
    return {
      axisX: 0,
      axisY: 0,
      axisThrottle: 1,
      left: false,
      right: false,
      currentAxisX: 0,
      currentAxisY: 0,
      damping: 0,

      lockForwardToggle: false,

      reverse: false,
      brake: false
    };
  },

  componentWillMount: function() {
    this.lastButtonPressed = [];
    this.buttonJustPressed = [];

    this.loop();
    this.interval = setInterval(() => this.sendData(), 50);
  },

  componentWillUnmount: function () {
    if (this.animFrame) {
        cancelAnimationFrame(this.animFrame);
    }

    clearInterval(this.interval);
  },

  sendData: function() {

    if (this.props.inputMode != "joystick") {
      return;
    }

    var deltaX = this.state.axisX - this.state.currentAxisX;
    var deltaY = this.state.axisY - this.state.currentAxisY;

    var damp = 1-this.state.damping/100;
    damp = damp*damp*damp;

    var newAxisX = this.state.currentAxisX + deltaX*damp;
    var newAxisY = this.state.currentAxisY + deltaY*damp;

    // console.log(newAxisX);

    var angle = 90;
    var speed = 0;


    angle = Math.atan2(newAxisY, newAxisX) / Math.PI * 180;

    if (angle < 0) {
      angle += 360;
    }
    var dist = Math.sqrt(newAxisX*newAxisX + newAxisY*newAxisY);
    var realDist = Math.sqrt(this.state.axisX*this.state.axisX + this.state.axisY*this.state.axisY);
    var pt = 0.6;

    if (realDist < 1-pt) {
      dist = 0;
      speed = 0;
      angle = 90;
    }

    if (dist < 1-pt) {
      dist = 0;
      speed = 0;
      angle = 90;
    } else {
      speed = Math.floor((dist/pt-(1-pt)) * 150);

      var throttle = -(this.state.axisThrottle - 1)/2;
      if (throttle < 0) {throttle = 0;}
      if (throttle > 1) {throttle = 1;}

      // throttle the speed
      speed = speed*throttle;

      if (speed > 191) {
        speed = 191
      }

      if (speed < 25) {
        speed = 0;
      }
    }

    if (speed == null) {
      speed = 0;
    }



    ///// (testing: x and y axis speed/angle control)
    // console.log((1-(this.state.axisX + 1)/2)*Math.PI);
    var throttle = -(this.state.axisThrottle - 1)/2;

    var axisYWithDeadzoneFix = (Math.abs(this.state.axisY) - 0.25) / (1-0.25);
    // console.log(axisYWithDeadzoneFix);
    // axisYWithDeadzoneFix = 1 * Math.pow( 2, 10 * (axisYWithDeadzoneFix/1 - 1) ) + 0;
    // axisYWithDeadzoneFix = 0.9071*axisYWithDeadzoneFix*axisYWithDeadzoneFix - 0.113*axisYWithDeadzoneFix;
    axisYWithDeadzoneFix = Math.abs(0.1479*Math.pow(axisYWithDeadzoneFix,4) - 0.3639*Math.pow(axisYWithDeadzoneFix,3) + 1.4183*Math.pow(axisYWithDeadzoneFix,2) - 0.2105*axisYWithDeadzoneFix);

    // axisYWithDeadzoneFix = 1 * ( -Math.pow( 2, -10 * axisYWithDeadzoneFix/1 ) + 1 ) + 0;
    // console.log(axisYWithDeadzoneFix);


    if (this.state.axisY < 0) {
      axisYWithDeadzoneFix *= -1;
    }

    var axisXWithDeadzoneFix = (Math.abs(this.state.axisX) - 0.25) / (1-0.25);

    if (this.state.axisX < 0) {
      axisXWithDeadzoneFix *= -1;
    }

    angle = ((1-(axisXWithDeadzoneFix + 1)/2)*Math.PI)
    speed = (axisYWithDeadzoneFix) * 191 * throttle;

    angle = angle / Math.PI*180;

    // dead zone for speed
    if (this.state.axisY < 0.25 && this.state.axisY > -0.25) {
      speed = 0;
    }

    // dead zone for angle
    if (this.state.axisX < 0.25 && this.state.axisX > -0.25) {
      angle = 90;
    }

    if (speed < 0 || this.state.reverse) {
      angle = 360 - angle;
      speed = Math.abs(speed);
    }

    // dead zone 2 (backup)
    // if (realDist < 0.2) {
      // angle = 90;
      // speed = 0;
    // }

    if (this.state.brake) {
      speed = 0;
    }

    if (this.state.lockForwardToggle) {
      if (angle <= 180) {
        angle = 90;
      } else {
        angle = 270;
      }
    }

    // yeah.. i converted it from rad to deg then back
    // sue me :^(
    // var startX = Math.cos(this.state.currentAngle/180*Math.PI);
    // var startY = Math.sin(this.state.currentAngle/180*Math.PI);

    // var goalX = Math.cos(angle/180*Math.PI);
    // var goalY = Math.sin(angle/180*Math.PI);

    // // console.log(goalX, goalY);

    // var dot = Math.abs(startX*goalX + startY*goalY);
    // var angDiff = Math.acos(dot);

    // var crossZ = startX*goalY - goalX*startY;
    // var sign = crossZ / Math.abs(crossZ);

    // if (crossZ == 0) {
    //   sign = 0;
    // }

    // var slerpAmount = 0.1;
    // var newAngle = (this.state.currentAngle + (angDiff / Math.PI * 180) * sign * slerpAmount + 360) % 360;

    // console.log(newAngle);
    // console.log(Math.floor(angDiff / Math.PI * 180));
    // console.log(sign);


    this.setState({
      currentAxisX: newAxisX,
      currentAxisY: newAxisY,
      joystickAngle: angle,
      joystickSpeed: Math.floor(speed/191*100)
    });

    this.props.netHandler.execute('driveSystem', {
      "name": "move",
      "mode": this.props.carMode,
      "speed": Math.floor(speed),
      //"angle": this.state.axisX * -45
      "angle": Math.floor(angle)
    });
  },
  loop: function() {
    var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : []);
    // console.log(gamepads);

    var foundGamepad = false;

    for (var gamepadId in gamepads) {
      var gamepad = gamepads[gamepadId];

      foundGamepad = true;

      if (!gamepad || !gamepad.id || !gamepad.id.toLowerCase().match(/logitech extreme/g)) {
        continue;
      }

      if (gamepad && gamepad.axes) {
        var axes = gamepad.axes;

        var axesThrottle = axes[6];

        if (axesThrottle != null && !this.firstAxesThrottle && !this.checkedChanged) {
          this.firstAxesThrottle = axesThrottle;
          this.checkedChanged = true;
        }

        if (axesThrottle == this.firstAxesThrottle) {
          axesThrottle = 1;
        } else {
          this.firstAxesThrottle = null;
        }

        // cross platform
        var button2 = false;
        if (gamepad.buttons && gamepad.buttons[2] && gamepad.buttons[2].pressed) {
          button2 = true;
        }

        var button3 = false;
        if (gamepad.buttons && gamepad.buttons[3] && gamepad.buttons[3].pressed) {
          button3 = true;
        }

        // try {
        //   if (gamepad.buttons && gamepad.buttons[0]) {
        //     this.setState({
        //       reverse: gamepad.buttons[0].pressed
        //     });
        //   }          
        // } catch(e) { }

        // try {
        //   if (gamepad.buttons && gamepad.buttons[0]) {
        //     this.setState({
        //       brake: gamepad.buttons[11].pressed
        //     });
        //   }          
        // } catch(e) { }

        /* drive system switching */
        try {
          if (gamepad.buttons[5].pressed) {
            this.props.setMode("crabwalk");
          }          
        } catch(e) { }

        try {
          if (gamepad.buttons[4].pressed) {
            this.props.setMode("car");
          }          
        } catch(e) { }

        try {
          if (gamepad.buttons[3].pressed) {
            this.props.setMode("rotate");
          }          
        } catch(e) { }

        // LOCK FORWARD
        try {
          if (gamepad.buttons[2].pressed && gamepad.buttons[2].pressed != this.lastLockButton) {
            this.setState({
              lockForwardToggle: !this.state.lockForwardToggle
            });
          }
          this.lastLockButton = gamepad.buttons[2].pressed;
        } catch(e) { }

        // ROLL SPEED CONTROL
        for (var x = 0; x <= gamepad.buttons.length; x++) {
          try {
            if (gamepad.buttons[x]) {
              this.buttonJustPressed[x] = gamepad.buttons[x].pressed && gamepad.buttons[x].pressed != this.lastButtonPressed[x];
              this.lastButtonPressed[x] = gamepad.buttons[x].pressed;
            }
          } catch (e) {}
        }

        // GRIP
        if (this.buttonJustPressed[0]) {
          this.props.setState({
            clawForce: 999
          });
        }

        // GRIP2
        if (this.buttonJustPressed[1]) {
          this.props.setState({
            clawForce: 0
          });
        }

        // left
        /// slow
        if (this.buttonJustPressed[10]) {
          this.props.setState({
            method: -1
          });
        }
        // normal
        if (this.buttonJustPressed[8]) {
          this.props.setState({
            method: -2
          });
        }
        // high
        if (this.buttonJustPressed[6]) {
          this.props.setState({
            method: -3
          });
        }

        // right
        /// slow
        if (this.buttonJustPressed[11]) {
          this.props.setState({
            method: 1
          });
        }
        // normal
        if (this.buttonJustPressed[9]) {
          this.props.setState({
            method: 2
          });
        }
        // high
        if (this.buttonJustPressed[7]) {
          this.props.setState({
            method: 3
          });
        }


        if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
          this.setState({
            axisX: axes[1],
            axisY: -axes[2],
            axisThrottle: -1,
            left: button2,
            right: button3,
          });
        } else {
          this.setState({
            axisX: axes[0],
            axisY: -axes[1],
            axisThrottle: axesThrottle,
            left: button2,
            right: button3,
          });
        }

        break;
      } else {
        break;
      }
    }

    if (!foundGamepad) {
      this.checkedChanged = false;
      this.firstAxesThrottle = null;
      this.setState({
        axisX: 0,
        axisY: 0,
        axisThrottle: 0,
        left: 0,
        right: 0,
      });
    }

    this.animFrame = requestAnimationFrame(() => this.loop());
  },

  changeDamping: function(value) {
    this.setState({
      damping: value.newValue
    });
  },

  render: function() {
    return <div>
      <hr />
      <div className="row">
        <div className="col-xs-12">
          <div className="row">
            <div className="col-xs-4">
              <h5>Forward Lock</h5>
            </div>

            <div className="col-xs-8">
              <h6><img src={this.state.lockForwardToggle ? "/images/lock.png" : "/images/unlock.gif"} /></h6>
            </div>
          </div>

          <div className="row">
            <div className="col-xs-4">
              <h5>Throttle</h5>
            </div>

            <div className="col-xs-8">
              <ProgressBar progress={Math.floor(-(this.state.axisThrottle - 1)/2*100)} color="info"/>
            </div>
          </div>

          <div className="row">
            <div className="col-xs-4">
              <h5>Speed</h5>
            </div>

            <div className="col-xs-8">
              <ProgressBar progress={this.state.joystickSpeed} color={(this.state.joystickAngle <= 180) ? "primary" : "danger"}/>
            </div>
          </div>
        </div>

        <KnobButton myAngle={this.state.joystickAngle}/>

        <div className="col-xs-6">
          X: {Math.floor(this.state.axisX*100)/100}
        </div>
        <div className="col-xs-6">
          Y: {Math.floor(this.state.axisY*100)/100}
        </div>
      </div>
    </div>
  }
});


var JoystickDisplayBox = React.createClass({
  /*<JoystickDisplayBox netHandler={this.netHandler}/>*/
  getInitialState: function() {

        return{
          buttons: [],

        };
    },

  componentDidMount: function() {
    var gamepadInfo = document.getElementById("gamepad-info");

    var buttonz = [document.getElementById("button0"), document.getElementById("button1"), document.getElementById("button2"), document.getElementById("button3"), 
                 document.getElementById("button4"), document.getElementById("button5"), document.getElementById("button6"), document.getElementById("button7"),
                 document.getElementById("button8"), document.getElementById("button9"), document.getElementById("button10"), document.getElementById("button11")];
        var axesz = [document.getElementById("axes0"), document.getElementById("axes1"), document.getElementById("axes2"), document.getElementById("axes3")];

    var buttonArr = [0,0,0,0,0,0,0,0,0,0,0,0,0]; 
    var axesArr = [0,0,0,0];
    var i = 0;

    var rAF = window.mozRequestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.requestAnimationFrame;

    var rAFStop = window.mozCancelRequestAnimationFrame ||
      window.webkitCancelRequestAnimationFrame ||
      window.cancelRequestAnimationFrame;

    window.addEventListener("gamepadconnected", function() {
      var gp = navigator.getGamepads()[0];
      gamepadInfo.innerHTML = "Gamepad connected at index " + gp.index;
      update();
    });

    window.addEventListener("gamepaddisconnected", function() {
      gamepadInfo.innerHTML = "Waiting for gamepad.";
      rAFStop(start);
    });

    if(!('GamepadEvent' in window)) {
      // No gamepad events available, poll instead.
      var interval = setInterval(pollGamepads, 500);
    }

    function pollGamepads() {
      var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : []);
      for (var i = 0; i < gamepads.length; i++) {
        var gp = gamepads[i];
        if(gp) {
          gamepadInfo.innerHTML = "Gamepad connected at index " + gp.index;
          update();
          clearInterval(interval);
        }
      }
    }

    function update() {
      var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : []);
      if (!gamepads)
        return;
      var gp = gamepads[0];

      for(i=0; i<12; i++){
        if(gp.buttons[i].pressed) {
          buttonArr[i] = 1;
        }
        else{
          buttonArr[i] = 0;
        }
        buttonz[i].innerHTML = buttonArr[i];
      }

      for(i=0; i<4; i++){
        axesArr[i] = gp.axes[i];
        axesz[i].innerHTML = axesArr[i].toFixed(4);
      }

      var start = rAF(update);
    };


  },

  componentWillUnmount: function() {
  },

  render: function() {
    return (
      <div className="displayBox" style={{border: '2px solid white'}}>
      Joystick Info Display Box
        <p id="gamepad-info">Waiting for Gamepad.</p>
        <table ref="table" style={{width: '250px', border: '1px solid white'}}>
                <thead>
                  <tr>
                    <th>Button</th>
                    <th>Value</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>TestButton</td>
                    <td>{this.state.buttons[0]}</td>
                  </tr>
                  <tr>
                    <td>Button0</td>
                    <td id="button0">NULL</td>
                  </tr>
                  <tr>
                    <td>Button1</td>
                    <td id="button1">NULL</td>
                  </tr>
                  <tr>
                    <td>Button2</td>
                    <td id="button2">NULL</td>
                  </tr>
                  <tr>
                    <td>Button3</td>
                    <td id="button3">NULL</td>
                  </tr>
                  <tr>
                    <td>Button4</td>
                    <td id="button4">NULL</td>
                  </tr>
                  <tr>
                    <td>Button5</td>
                    <td id="button5">NULL</td>
                  </tr>
                  <tr>
                    <td>Button6</td>
                    <td id="button6">NULL</td>
                  </tr>
                  <tr>
                    <td>Button7</td>
                    <td id="button7">NULL</td>
                  </tr>
                  <tr>
                    <td>Button8</td>
                    <td id="button8">NULL</td>
                  </tr>
                  <tr>
                    <td>Button9</td>
                    <td id="button9">NULL</td>
                  </tr>
                  <tr>
                    <td>Button10</td>
                    <td id="button10">NULL</td>
                  </tr>
                  <tr>
                    <td>Button11</td>
                    <td id="button11">NULL</td>
                  </tr>
                  <tr>
                    <td>Axes-Y</td>
                    <td id="axes0">NULL</td>
                  </tr>
                  <tr>
                    <td>Axes-X</td>
                    <td id="axes1">NULL</td>
                  </tr>
                  <tr>
                    <td>Axes-Z</td>
                    <td id="axes2">NULL</td>
                  </tr>
                  <tr>
                    <td>Axes+-</td>
                    <td id="axes3">NULL</td>
                  </tr>
                </tbody>
              </table>
        </div>
    );
  }
});

var IncomingDisplayBox = React.createClass({

  getInitialState: function() {
    return{
      left: 'Left',
      right: 'Right',
      pirateOn: 'n'
    };
  },    

  componentDidMount: function() {
    $("#accordian ul ul").slideUp();
    $("#accordian h3").click(function(){
      $(this).next().slideToggle();
    });
    $(document.body).on('keydown', this.handleKeyDown);

  },

  componentWillUnmount: function() {
    $(document.body).off('keydown', this.handleKeyDown);
  },

  handleKeyDown: function(e) {
    var P = 80;
    if( e.keyCode == P) {
      if(this.state.pirateOn=='n'){
        this.setState({left:'Port', right:'Starboard', pirateOn: 'y'});
      }
      else{
        this.setState({left:'Left', right:'Right', pirateOn: 'n'});
      }
    }
  },

  render: function() {
    return (
      <div className="displayBox" style={{margin: '5px 5px 5px 0px'}}>
      
        <div id="accordian">
          <ul>
            <li>
              <h3><span className="speed"></span>Speed</h3>
              <ul>
                <li>
                  <table className="table" style={{width: '100%', border: '2px solid white'}}>
                    <thead >
                      <tr>
                        <th style={{width: '50%', border: '2px solid white'}}>{this.state.left}</th>
                        <th style={{width: '50%', border: '2px solid white'}}>{this.state.right}</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr style={{border: '2px solid white'}}>
                        <td style={{border: '2px solid white'}}>{this.props.frontLeft}</td>
                        <td style={{border: '2px solid white'}}>{this.props.frontRight}</td>
                      </tr>
                      <tr style={{border: '2px solid white'}}>
                        <td style={{border: '2px solid white'}}>{this.props.backLeft}</td>
                        <td style={{border: '2px solid white'}}>{this.props.backRight}</td>
                      </tr>
                    </tbody>
                  </table>
                </li>

              </ul>
            </li>
            <li>
              <h3>Torque</h3>
              <ul>
                <li>
                  <table className="table" style={{width: '100%', border: '2px solid white'}}>
                    <thead >
                      <tr>
                        <th style={{width: '50%', border: '2px solid white'}}>{this.state.left}</th>
                        <th style={{width: '50%', border: '2px solid white'}}>{this.state.right}</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr style={{border: '2px solid white'}}>
                        <td style={{border: '2px solid white'}}>{this.props.frontLeft}</td>
                        <td style={{border: '2px solid white'}}>{this.props.frontRight}</td>
                      </tr>
                      <tr style={{border: '2px solid white'}}>
                        <td style={{border: '2px solid white'}}>{this.props.backLeft}</td>
                        <td style={{border: '2px solid white'}}>{this.props.backRight}</td>
                      </tr>
                    </tbody>
                  </table>
                </li>
              </ul>
            </li>
            <li>
              <h3>Power</h3>
              <ul>
                <li>
                  <table className="table" style={{width: '100%', border: '2px solid white'}}>
                    <thead >
                      <tr>
                        <th style={{width: '50%', border: '2px solid white'}}>{this.state.left}</th>
                        <th style={{width: '50%', border: '2px solid white'}}>{this.state.right}</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr style={{border: '2px solid white'}}>
                        <td style={{border: '2px solid white'}}>{this.props.frontLeft}</td>
                        <td style={{border: '2px solid white'}}>{this.props.frontRight}</td>
                      </tr>
                      <tr style={{border: '2px solid white'}}>
                        <td style={{border: '2px solid white'}}>{this.props.backLeft}</td>
                        <td style={{border: '2px solid white'}}>{this.props.backRight}</td>
                      </tr>
                    </tbody>
                  </table>
                </li>
              </ul>
            </li>
          </ul>
        </div>

      </div>
    );
  }
});

export var route = {
  name: "Navigation",
  link: "#/Navigation"
};

/* Export our newly created page so that the world can see it! */
export default HomePage;