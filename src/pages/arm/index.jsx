
import ProgressBar from '../../components/ProgressBar.jsx';
import VerticalSlider from '../../components/VerticalSlider.jsx';
import Slider from '../../components/Slider.jsx';
import Toggle from '../../components/Toggle.jsx';

import RoverViewer from './RoverViewer.jsx';

import NetHandler from "../../network/NetHandler.jsx";

console.log(RoverViewer);

function clamp(val, min, max) {
	return Math.max(Math.min(val, max), min);
}
var btn_width = {
	width: "12%",
	marginLeft: ".5%",
	marginRight: ".5%"
}

var HomePage = React.createClass({

	/* Create our Model */
	getInitialState: function() {
		return {
			x: 50, y: -50, z: 50,
			progress: 40,
			length1: 40,
			length2: 40,
			angle1: Math.PI/4,
			angle2: Math.PI,
			angle3: Math.PI/2,
			angle4: Math.PI/2,

			/* Angles for Udit's Manual Control */
			manAngle1: 90,
			manAngle2: 90,
			manAngle3: 90,
			manAngle4: 0,

			handAngle: 0,
			rollAngle: 180,
			clawForce: 999,
			goalAngle1: 0,
			goalAngle2: 0,
			goalAngle3: 0,
			lerp: 50,
			manualControl: false,
			method: 0,
		};
	},
	
	_handleKey:function(event){
        //console.log(event);
        const UP = 40;
        const DOWN = 38;
        const RIGHT = 39;
        const LEFT = 37;
        const MOVE = 10;
        const CTRL = 17;
        const ALT = 18;
        switch(event.keyCode) {
	        case UP:
		        if(this.state.handAngle+MOVE >= 90) {
					this.setState({ handAngle: 90 });
	        	} else {
		        	this.setState({ handAngle: this.state.handAngle+MOVE});
	        	}
	        	console.log(this.state.handAngle);
	        	break;
	        case DOWN:
		        if(this.state.handAngle-MOVE <= -90) {
		        	this.setState({ handAngle: -90});
	        	} else {
		        	this.setState({ handAngle: this.state.handAngle-MOVE});
	        	}
	        	console.log(this.state.handAngle);
	        	break;
	        case RIGHT:
		        if(this.state.rollAngle+MOVE >= 360) {
		        	this.state.rollAngle = 360;
	        	} else {
		        	this.state.rollAngle += MOVE;
	        	}
	        	console.log(this.state.rollAngle);
	        	break;
	        case LEFT:
		        if(this.state.rollAngle-MOVE <= 0) {
		        	this.state.rollAngle = 0;
	        	} else {
		        	this.state.rollAngle -= MOVE;
	        	}
	        	console.log(this.state.rollAngle);
	        	break;
	        case CTRL:
	        	// GRAB SOMETHING
		        this.state.clawForce = 0;
	        	console.log(this.state.clawForce);
	        	break;
	        case ALT:
	        	// GRAB SOMETHING
		        this.state.clawForce = 999;
	        	console.log(this.state.clawForce);
	        	break;
	        default:
		        console.log("invalid keyinput ", event.keyCode);
	        	break;
        }
   },

	componentWillMount:function(){
		//HomePage.addChangeListener(this._onchange);
		document.addEventListener("keyup", this._handleKey, false);
	},

	/* Render our View */
	render: function() {
		return (
			<div>
				<div className="row">
  				<div className="col-md-3">Temperature: {this.state.temperature}</div>
  				<div className="col-md-3">Base: {this.state.base}</div>
  				<div className="col-md-3">Shld: {this.state.shld}</div>
					<div className="col-md-3">Elbow: {this.state.elbow}</div>
				</div>
				<div className="row">
					<div className="col-md-12">Wrist: {this.state.temperature}</div>
				</div>
				<div className="row">
					<div className="col-xs-6">
						<RoverViewer
							pos={{x: this.state.x, y: this.state.y, z: this.state.z}}
							length1={this.state.length1}
							length2={this.state.length2}
							ang1={this.state.angle1}
							ang2={this.state.angle2}
							ang3={this.state.angle3}
							ang4={this.state.angle4}
							onChange={this.onTranslateGoal}
							orbit="true"
							cameraPos={{x: 0, y: 0, z: 100}}
							cameraRot={{x: 0, y: 0, z: 0}}
						/>
					</div>

					<div className="col-xs-6">
						<p>Wrist Angle</p>
						<Slider initialValue={this.state.handAngle} min="0" max="90" hideTooltip="true" onChange={this.onHandAngleChange}/>
						<hr/>

						<div ref="manualControl" style={{display: 'inline'}}>
							<p>Shoulder (Manual)</p>
							<Slider ref="manAngle1" min="0" max="180" initialValue={90} onChange={this.onSliderChange1}/>
							<p>Elbow (Manual)</p>
							<Slider ref="manAngle2" min="0" max="180" initialValue={90} onChange={this.onSliderChange2}/>
							<p>Base (Manual)</p>
							<Slider ref="manAngle3" min="0" max="180" initialValue={90} onChange={this.onSliderChange3}/>
							<p>Wrist (Manual)</p>
							<Slider ref="manAngle4" min="0" max="180" initialValue={0} onChange={this.onSliderChange4}/>
							<button type="button" className="btn btn-success" onClick={this.onGripClick}>Grip (an object with the hand)</button>&nbsp;
							<button type="button" className="btn btn-success" onClick={this.onReleaseClick}>Release</button>&nbsp;
						</div>
						Manual Control:&nbsp;
						<Toggle onChange={this.onToggleManualControl}/>
						<br />
						<br />
					</div>
				</div>
				<div className="row">
					<div className="col-xs-6">
						<RoverViewer
							pos={{x: this.state.x, y: this.state.y, z: this.state.z}}
							length1={this.state.length1}
							length2={this.state.length2}
							ang1={this.state.angle1}
							ang2={this.state.angle2}
							ang3={this.state.angle3}
							ang4={this.state.angle4}
							cameraPos={{x: -100, y: 40, z: 0}}
							cameraRot={{x: 0, y: -Math.PI/2, z: 0}}
						/>
					</div>
					
					<div className="col-xs-6">
						<RoverViewer
							pos={{x: this.state.x, y: this.state.y, z: this.state.z}}
							length1={this.state.length1}
							length2={this.state.length2}
							ang1={this.state.angle1}
							ang2={this.state.angle2}
							ang3={this.state.angle3}
							ang4={this.state.angle4}
							cameraPos={{x: 0, y: 150, z: 0}}
							cameraRot={{x: -Math.PI/2, y: 0, z: 0}}
						/>
					</div>
				</div>
        <div className="row">
          <div className="col-xs-4">
            <p> Wrist Pitch Angle </p>
            <Slider initialValue={this.state.handAngle} min="-90" max="90" hideTooltip="true" onChange={this.onHandAngleChange}/>
          </div>
          <div className="col-xs-4">
            <p> Wrist Roll Angle </p>
            <Slider initialValue={this.state.rollAngle} min="0" max="360" hideTooltip="true" onChange={this.onRollAngleChange}/>
          </div>
          <div className="col-xs-4">
            <p> Wrist Claw Position </p>
            <Slider initialValue={this.state.clawForce} min="0" max="999" hideTooltip="true" onChange={this.onClawChange}/>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-2">
            <button onClick={this.methodEvent} method="-3" className="btn btn-default btn-block">Left Roll High Speed</button>
          </div>
          <div className="col-xs-1">
            <button onClick={this.methodEvent} method="-2" className="btn btn-default btn-block">Left Roll</button>
          </div>
          <div className="col-xs-1">
            <button onClick={this.methodEvent} method="-1" className="btn btn-default btn-block">Left Roll Slow</button>
          </div>
          <div className="col-xs-2">
            <button onClick={this.methodEvent} method="0" className="btn btn-default btn-block">Pitch Only</button>
          </div>
          <div className="col-xs-1">
            <button onClick={this.methodEvent} method="1" className="btn btn-default btn-block">Right Roll Slow</button>
          </div>
          <div className="col-xs-1">
            <button onClick={this.methodEvent} method="2" className="btn btn-default btn-block">Right Roll</button>
          </div>
          <div className="col-xs-2">
            <button onClick={this.methodEvent} method="3" className="btn btn-default btn-block">Right Roll High Speed</button>
          </div>
          <div className="col-xs-2">
            <button onClick={this.methodEvent} method="4" className="btn btn-default btn-block">Roll Only</button>
          </div>
       	</div>

       	<br />

        <div className="row">
          <div className="col-xs-2">
            <button onClick={this.methodEvent} method="5" className="btn btn-default btn-block">Servos Off</button>
          </div>
          <div className="col-xs-2">
            <button onClick={this.methodEvent} method="6" className="btn btn-warning btn-block">50% Power</button>
          </div>
          <div className="col-xs-2">
            <button onClick={this.methodEvent} method="7" className="btn btn-success btn-block">100% Power</button>
          </div>
				</div>
			</div>
		)
	},

	toggleManual: function(enabled) {
		if (enabled) {
			this.refs.manualControl.style.color = "inherit";
			this.refs.manualControl.style.opacity = 1;
			this.refs.manualControl.style.pointerEvents = "auto";
		} else {
			this.refs.manualControl.style.color = "#333";
			this.refs.manualControl.style.opacity = 0.5;
			this.refs.manualControl.style.pointerEvents = "none";
		}
	},

	onToggleManualControl: function(e) {
		this.toggleManual(e.newValue);
		this.setState({
			manualControl: e.newValue
		})
	},

	/* Udit's Manual Controls */

	onSliderChange1: function(value) {
		this.setState({
			manAngle1: value.newValue * Math.PI / 180
		});
	},

	onSliderChange2: function(value) {
		this.setState({
			manAngle2: value.newValue * Math.PI / 180
		});
	},

	onSliderChange3: function(value) {
		this.setState({
			manAngle3: value.newValue * Math.PI / 180
		});
	},

	onSliderChange4: function(value) {
		this.setState({
			manAngle4: value.newValue * Math.PI / 180
		});
	},

	/* Controller Functions */
	onTranslateGoal: function(x, y, z) {
		this.setState({
			x: x,
			y: y,
			z: z
		})
	},
	
	methodEvent: function(button) {
		this.setState({ 
			method: parseInt(button.target.attributes.method.nodeValue)
		});
	},

	onXSlideChange: function(value) {
		this.setState({
			x: value.newValue
		});
	},


	onYSlideChange: function(value) {
		this.setState({
			y: value.newValue
		});
	},


	onZSlideChange: function(value) {
		this.setState({
			z: value.newValue
		});
	},

	onSliderChange: function(value) {
		this.setState({
			progress: value.newValue
		});
	},

	onLerpChange: function(value) {
		this.setState({
			lerp: value.newValue
		})
	},

	onHandAngleChange: function(value) {
		this.setState({
			handAngle: value.newValue
		})
	},
	onRollAngleChange: function(value) {
		console.log(value);
		this.setState({
			rollAngle: value.newValue
		})
	},
	onClawChange: function(value) {
		console.log(value);
		this.setState({
			clawForce: value.newValue
		})
	},

	moveMouse: function(event) {
		// var canvas = this.refs.canvas;
		// var rect = canvas.getBoundingClientRect();
		// this.move(event.clientX - rect.left, event.clientY - rect.top);
	},

	componentDidUpdate: function() {

	},

	calculateIK: function(x, y, z) {

		/*    b    _.'gamma
			    _.'  |\ a
			_.'      | \
		alpha------------*-beta
			            P1
			      c
			
		*/

		// motor1Angle = angle of motor 1
		// gamma = angle of motor 2

		var length1 = this.state.length1;
		var length2 = this.state.length2;

		var a = length2;
		var b = length1;
		var c = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));

		var alpha = Math.acos((b*b + c*c - a*a)/(2*b*c));
		var gamma = Math.acos((a*a + b*b - c*c)/(2*a*b));

		var delta = Math.atan2(y, x);

		if (c >= (a+b)) {
			alpha = 0;
			gamma = Math.PI;
			// delta = 0;
		}

		// gamma = clamp(gamma, Math.PI/4, Math.PI);

		var gammaPrime = Math.PI - gamma;

		var motor1Angle = alpha - delta;

		// motor1Angle = clamp(motor1Angle, -Math.PI/4, Math.PI * (3/4));
		alpha = motor1Angle + delta;

		return { angle1: motor1Angle, angle2: gamma };
	},

	componentDidMount: function() {
	
		this.toggleManual(false);

		this.netHandler = new NetHandler("ARM");
		this.netHandler.listen((changes) => {
			// this.setState(changes);
			// console.log(changes)
		});

		if (typeof Leap != "undefined") {
	 		this.leapController = Leap.loop({}, (frame) => {
				if (frame.hands[0]) {
					// console.log(frame.hands[0].palmPosition);
					var pos = frame.hands[0].palmPosition;
					this.setState({
						x: -pos[2]/2 + 50,
						y: -pos[1]/2 + 50,
						z: pos[0]/2
					})
				}
			});
	 	}

		this.oldAngle1 = 0;
		this.oldAngle2 = 0;
		this.oldAngle3 = 0;
		this.oldAngle4 = 0;


		this.lerper = setInterval(() => {
			var baseAngle = Math.atan2(this.state.x, this.state.z) || 0;
			var length = Math.sqrt(Math.pow(this.state.x, 2) + Math.pow(this.state.z, 2));

			//baseAngle = clamp(baseAngle, 0, Math.PI);

			console.log(baseAngle);
			var y = this.state.y;

			if (baseAngle < 0) {
				baseAngle = Math.PI+baseAngle;
				// baseAngle = -baseAngle;
				length = -length;
			}

			var curAngle1 = this.state.angle1 || 0;
			var curAngle2 = this.state.angle2 || 0;
			var curAngle3 = this.state.angle3 || 0;

			var handAngle = (Math.PI-curAngle1) - curAngle2 - this.state.handAngle/180 * Math.PI;

			var angles;

			if (this.state.manualControl) {
				angles = {
					angle1: this.state.manAngle1,
					angle2: this.state.manAngle2,
					angle3: this.state.manAngle3,
					angle4: this.state.manAngle4
				}

				baseAngle = this.state.manAngle3;
				handAngle = this.state.manAngle4;
			} else {
				angles = this.calculateIK(length, this.state.y);
			}

			var newState = {
				angle1: curAngle1 + ((angles.angle1 || this.state.goalAngle1 || 0) - curAngle1) * this.state.lerp / 100,
				angle2: curAngle2 + ((angles.angle2 || this.state.goalAngle2 || 0) - curAngle2) * this.state.lerp / 100,
				angle3: curAngle3 + ((baseAngle || this.state.goalAngle3 || 0) - curAngle3) * this.state.lerp / 100,
				angle4: handAngle,
				angle5: 0,
				goalAngle1: (angles.angle1 || this.state.goalAngle1 || 0),
				goalAngle2: (angles.angle2 || this.state.goalAngle2 || 0),
				goalAngle3: (baseAngle || this.state.goalAngle3 || 0),
			};

			if (
				Math.abs(newState.angle1 - this.oldAngle1) < 0.01 &&
				Math.abs(newState.angle2 - this.oldAngle2) < 0.01 &&
				Math.abs(newState.angle3 - this.oldAngle3) < 0.01 &&
				Math.abs(newState.angle4 - this.oldAngle4) < 0.01
			) {
				return;
			}

			this.oldAngle1 = newState.angle1;
			this.oldAngle2 = newState.angle2;
			this.oldAngle3 = newState.angle3;
			this.oldAngle4 = newState.angle4;

			this.setState(newState);

			if (!this.state.manualControl) {
				this.refs.manAngle1.setVal(Math.floor(this.state.angle1 / Math.PI * 180));
				this.refs.manAngle2.setVal(Math.floor(this.state.angle2 / Math.PI * 180));
				this.refs.manAngle3.setVal(Math.floor(this.state.angle3 / Math.PI * 180));
				this.refs.manAngle4.setVal(Math.floor(this.state.angle4 / Math.PI * 180));

				this.setState({
					manAngle1: this.state.angle1,
					manAngle2: this.state.angle2,
					manAngle3: this.state.angle3,
					manAngle4: this.state.angle4
				});
			}

		}, 100);

		this.sendState = setInterval(() => {
			this.netHandler.execute("Arm", {
				"rotonda": Math.floor(this.state.angle3 / Math.PI * 360),
				"base": Math.floor(this.state.angle1 / Math.PI * 360),
				"elbow": Math.floor(this.state.angle2 / Math.PI * 360),
				"pitch": this.state.handAngle,
				"method": this.state.method,
				"roll": this.state.rollAngle,
				"claw": this.state.clawForce,
				"laser": 1,
			});
		}, 100);
	},

	componentWillUnmount: function() {
		document.removeEventListener("keyup", this._handleKey, false);
		clearInterval(this.lerper);
		clearInterval(this.sendState);
		
		if (this.leapController)
			this.leapController.disconnect();
	}

});

export var route = {
	name: "Arm",
	link: "#/arm"
};

/* Export our newly created page so that the world can see it! */
export default HomePage;
