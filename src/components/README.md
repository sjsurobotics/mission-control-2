# What's in here?

In this folder you'll see a collection of React components that can be reused elsewhere. Take note, these are for **common** components, such as buttons, sliders, toggles, progess bars.