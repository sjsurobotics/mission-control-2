
export default class DummyArm {
	constructor() {
		this.commands = {
			"rotate": {
				angle1: Number,
				angle2: Number
			}
		}

		this.motors = [
			{theta: 0, torque: false},
			{theta: 0, torque: false},
			{theta: 0, torque: false}
		];
	}
}
