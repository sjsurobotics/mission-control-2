'use strict';

var express = require('express');
var Primus = require('primus')
var http = require('http');

var app = express();
app.use('/', express.static(__dirname + '/public'));
var server = http.createServer(app);

server.listen(8000);

var primus = new Primus(server, {});
var value = true;

var spawn = require('child_process').spawn
var readline = require('readline');


var child1 = spawn('wget', ['-T', '1', '-O', 'video.mjpeg', '127.0.0.1:9001']);
var newStream1On = true;
var oldStream1On;
var counter1 = 0;

var updateCameraStatus = function(camera, status) {
	primus.write(
		{
		'target': 'stream',
		'camera': camera,
		'status': status
		}
	);
}

child1.stdout.on('data', function(data) {
    console.log('stdout: ' + data);
});
child1.stderr.on('data', function(data) {
	if (counter1 === 0) {
        primus.write(newStream1On);
        counter1 += 1
    }
    oldStream1On = newStream1On;
    if (data.toString().match("Connection timed out")) {
        newStream1On = false;
    }  else if (data.toString().match(/\. [0-9]+\.[0-9]+M/g)) {
        newStream1On = true
    }
    if (oldStream1On !== newStream1On) {
        updateCameraStatus(1, newStream1On);
        console.log('sending')
    }
});
child1.on('close', function(code) {
    console.log('closing code: ' + code);
});

var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var spawn = require('child_process').spawn;
var child2 = spawn('wget', ['-T', '1', '-O', 'video.mjpeg', '127.0.0.1:9003']);
var newStream2On = true;
var oldStream2On;
var counter2 = 0;


primus.on('connection', function (spark) {
	spark.write(newStream1On);
	spark.write(newStream2On);
});

child2.stdout.on('data', function(data) {
    console.log('stdout: ' + data);
});
child2.stderr.on('data', function(data) {
	if (counter2 === 0) {
        updateCameraStatus(2, newStream2On);
        counter2 += 1
    }
    oldStream2On = newStream2On;
    if (data.toString().match("Connection timed out")) {
        newStream2On = false;
    }  else if (data.toString().match(/\. [0-9]+\.[0-9]+M/g)) {
        newStream2On = true;
    }
    if (oldStream2On !== newStream2On) {
        updateCameraStatus(2, newStream2On);
    }
});
child2.on('close', function(code) {
    console.log('closing code: ' + code);
});

var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
